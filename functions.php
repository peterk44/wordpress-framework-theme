<?php

class WordpressFrameworkTheme
{
    public function __construct()
    {
        add_action('init', array($this, 'getConfigs'));
        add_action('wp_title', array($this, 'resetQuery'));
    }
    
    public function getConfigs()
    {
        require_once(BASEPATH . 'app' . DS . 'config' . DS . 'wordpressConfigs.php');
    }

    public function resetQuery(){    
        global $wp_query;
        wp_reset_query();
    }    
}

new WordpressFrameworkTheme();